# LogParse: A weekend coding project in Go

## Something to know before proceeding

I am not an _"expert"_ with GoLang. Part of the purpose of this coding project was to stretch my skills. It's entirely
likely I made mistakes that would be obvious to a _Go Expert_. Feel free to open issues on the code for the suggestions
or improvements you have.

## Goals

Using a simple log format write code which can extract details from the log file. In particular the following scenarios
need to be supported:

- Search for instances of user in the log file
- See all users who were found in the log file
- See requests of a given size
- See users who are found in the log file on a given date

### Log File Format

For the purpose of this coding exercise the log file is given the following structure:

```csv
timestamp,username,operation,size
Sun Apr 01 22:21:20 UTC 2022,bob,download,29
```

#### A few things to note

- The file is expected to always have a header
- The CSV format does not support spaces between commas
- The date/time format is a `Unix Date` format. This can be parsed by Go natively
    - For more info [See this Go Doc](https://pkg.go.dev/time#Layout)

## Running the code

### First build the code

```bash
go build ./cmd/log-parse
```

### Now run the code

#### Search for a user

```bash
./log-parse summary user jeff22 --log-file "./log.csv"
```

#### Search for all users

```bash
./log-parse summary user --log-file "./log.csv"
```

#### Search for requests of a given size

```bash
./log-parse summary size 50 --operator gt --log-file "./log.csv"
./log-parse summary size 50 --operator lt --log-file "./log.csv"
./log-parse summary size 50 --operator eq --log-file "./log.csv"
```

#### Search for info on a specific date

```bash
./log-parse summary user jeff22 --log-file "./log.csv" --on-date "Mon Apr 13 01:45:46 UTC 2020"
./log-parse summary user --log-file "./log.csv" --on-date "Mon Apr 13 01:45:46 UTC 2020"
./log-parse summary size 22 --log-file "./log.csv" --on-date "Mon Apr 13 01:45:46 UTC 2020"
```

Note: Even though `time` info is specified - the filtering is based on the day not the hour/minute/second

## Design Objectives / Code Architecture

- Support an easy-to-use argument structure (provided by the `cobra cli` module)
- Inject dependencies where-ever possible to support package decoupling + promote unit testing
    - This solution does not (currently) use a formal DI solution, but it might be worth considering in the future
- Use a `chain of responsibility` design pattern as a way to make it simple for the code to determine what functionality
  should execute based on the input arguments.

### Solution Overview

```text
+---------------+
|               |
|   log-parse   |
|      app      |
+---+-----------+
    |
    |       +---------------+
    +-------+               |
    |       |      cmd      |
    |       |               |
    |       +-+-------------+
    |         |
    |         |   +---------------+
    |         +---+               |
    |             |   log-parse   |
    |             |               |
    |             +-+-------------+
    |               |
    |               |   +---------------+
    |               +---+               |
    |                   |     cmd       |  <--CLI for Application
    |                   |               |
    |                   +---------------+
    |
    |
    |        +---------------+
    +--------+               |
    |        | internal/pkg  |  <--Domain objects
    |        |               |
    |        +---------------+
    |
    |
    |        +---------------+
    |        |               |
    +--------+      pkg      |
    |        |               |
    |        +-+-------------+
    |          |
    |          |  +---------------+
    |          +--+               |
    |          |  |   handlers    |  <--Chain of handlers for processing input
    |          |  |               |
    |          |  +---------------+
    |          |
    |          |
    |          |  +---------------+
    |          +--+               |
    |          |  |   processor   |  <--Parsing code for CSV
    |          |  |               |
    |          |  +---------------+
    |          |
    |          |
    |          |  +---------------+
    |          +--+               |
    |          |  |    request    |  <--Input parameters mapped to actions to perform
    |          |  |               |
    |          |  +---------------+
    |          |
    |          |
    |          |  +---------------+
    |          +--+               |
    |             |   response    |  <--Response / Result types
    |             |               |
    |             +---------------+
    |
    |
    |       +---------------+
    +-------+               |
            |     Test      |  <--Unit test code
            |               |
            +---------------+
```

## Future Improvements / Functionality That's Missing

With this kind of coding project there always seems like there is more that you could do. However, the following
features that are missing but would add robustness to the solution:

- The format of the CSV file should be data driven instead of hard coded.
    - A config file should be used to identify columns, column datatype...etc


- LogParse was built to support a `search` mode in addition to the `summary` mode that exists currently.
    - The `search` mode would allow the extraction of log lines that match the same search conditions used by the
      current `summary` mode


- The current solution **needs** more unit tests. The core functionality is currently covered by two sets of unit tests.
  While this works, and verifies the required functionality - it would be better to have unit tests which target
  other `packages` in this solution.

