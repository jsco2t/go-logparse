package domain_test

import (
	"gitlab.com/jsco2t/go-logparse/internal/pkg/domain"
	"strings"
	"testing"
	"time"
)

func TestLogLineProcessing(t *testing.T) {
	csvData := []string{
		"Sun Apr 12 22:10:38 UTC 2020,sarah94,download,34",
		"Sun Apr 12 22:35:06 UTC 2020,Maia86,download,75",
		"Sun Apr 12 22:49:47 UTC 2020,Maia86,upload,a",
		"foobar,jeff22,download,25",
	}

	testCases := []struct {
		name   string
		exp    domain.LogLine
		expErr bool
	}{
		{
			name: "ParseSuccess1",
			exp: domain.LogLine{
				TimeStamp: time.Date(
					2020,
					4,
					12,
					0,
					0,
					0,
					0,
					time.UTC,
				),
				Username:  "sarah94",
				Operation: "download",
				Size:      34,
			},
		},
		{
			name: "ParseSuccess2",
			exp: domain.LogLine{
				TimeStamp: time.Date(
					2020,
					4,
					12,
					0,
					0,
					0,
					0,
					time.UTC,
				),
				Username:  "Maia86",
				Operation: "download",
				Size:      75,
			},
		},
		{
			name: "SizeColInvalid",
			exp: domain.LogLine{
				TimeStamp: time.Date(
					2020,
					4,
					12,
					0,
					0,
					0,
					0,
					time.UTC,
				),
				Username:  "Maia86",
				Operation: "upload",
				Size:      75,
			},
			expErr: true,
		},
		{
			name: "DateColInvalid",
			exp: domain.LogLine{
				TimeStamp: time.Date(
					2020,
					4,
					12,
					0,
					0,
					0,
					0,
					time.UTC,
				),
				Username:  "jeff22",
				Operation: "upload",
				Size:      25,
			},
			expErr: true,
		},
	}

	for i, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			res, err := domain.LogLineHandler(strings.Split(csvData[i], ","))

			if tc.expErr == true {
				if err == nil {
					t.Errorf("Expected error, got nil")
				}

				return // if we are testing error conditions stop here
			}

			if err != nil {
				t.Errorf("Unexpected error: %q", err)
			}

			res.TimeStamp = time.Date(
				res.TimeStamp.Year(),
				res.TimeStamp.Month(),
				res.TimeStamp.Day(),
				0,
				0,
				0,
				0,
				time.UTC,
			)

			if res != tc.exp {
				t.Errorf("Expected %v, got %v", tc.exp, res)
			}
		})
	}
}
