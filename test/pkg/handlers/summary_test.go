package handlers_test

import (
	"bytes"
	"gitlab.com/jsco2t/go-logparse/pkg/handlers"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"strings"
	"testing"
)

func TestSummaryHandler(t *testing.T) {
	testCases := []struct {
		name        string
		context     *request.RequestContext
		exp         string
		expContains string
		expErr      bool
	}{
		{
			name: "UserSummary",
			context: &request.RequestContext{
				Action: request.Summary,
				Filters: request.Filters{
					UserFilter: "jeff22",
				},
				FilePath: "../../data/test.csv",
			},
			exp: "[SUMMARY RESULTS]\nUsers summary: (users found: 1)\n\tName: jeff22, Result: 106 (request count)\n\n",
		},
		// the Map used to store the users can return the same set of users with
		// a random order - for perf reasons we don't want to force the map to be
		// sorted, so we loosen the test here to verify functionality
		{
			name: "AllUsersSummaryWithDate",
			context: &request.RequestContext{
				Action: request.Summary,
				Filters: request.Filters{
					DateFilter: "Mon Apr 13 01:45:46 UTC 2020",
				},
				FilePath: "../../data/test.csv",
			},
			expContains: "[SUMMARY RESULTS]\nUsers summary: (users found: 6)",
		},
		{
			name: "SizeSummary",
			context: &request.RequestContext{
				Action: request.Summary,
				Filters: request.Filters{
					SizeFilter: 50,
				},
				FilePath: "../../data/test.csv",
			},
			exp: "[SUMMARY RESULTS]\n--no user matches found--\nRequest size summary: \n\tFilter size of 50, Result: 369 (request count 'lt' filter)\n\n",
		},
		{
			name: "SizeGTSummary",
			context: &request.RequestContext{
				Action: request.Summary,
				Filters: request.Filters{
					SizeFilter: 50,
					Operator:   request.GT,
				},
				FilePath: "../../data/test.csv",
			},
			exp: "[SUMMARY RESULTS]\n--no user matches found--\nRequest size summary: \n\tFilter size of 50, Result: 277 (request count 'gt' filter)\n\n",
		},
		{
			name: "EmptyCSVFails",
			context: &request.RequestContext{
				Action: request.Summary,
				Filters: request.Filters{
					SizeFilter: 50,
					Operator:   request.GT,
				},
				FilePath: "../../data/empty.csv",
			},
			exp:    "",
			expErr: true,
		},
		{
			name: "MissingCSVFails",
			context: &request.RequestContext{
				Action: request.Summary,
				Filters: request.Filters{
					SizeFilter: 50,
					Operator:   request.GT,
				},
				FilePath: "../../data/missing.csv",
			},
			exp:    "",
			expErr: true,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			var res bytes.Buffer
			handler := handlers.SummaryHandler{}
			err := handler.Handle(tc.context, &res)

			if tc.expErr == true {
				if err == nil {
					t.Errorf("Expected error, got nil")
				}

				return // if we are testing error conditions stop here
			}

			if err != nil {
				t.Errorf("Unexpected error: %q", err)
			}

			if len(tc.expContains) > 0 {
				if !strings.Contains(res.String(), tc.expContains) {
					t.Errorf("Expected substring %v, not found in %v", tc.expContains, res.String())
				}
			} else {
				if res.String() != tc.exp {
					t.Errorf("Expected %v, got %v", tc.exp, res.String())
				}
			}
		})
	}
}
