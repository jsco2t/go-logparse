package errors

import "errors"

var (
	ErrInvalidNumColumn  = errors.New("invalid number of columns")
	ErrInvalidCsvFile    = errors.New("parsing failed for csv file")
	ErrFileEmpty         = errors.New("input file is empty")
	ErrInvalidReqContext = errors.New("invalid request context: search or summary are required")
)
