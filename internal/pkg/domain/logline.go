package domain

import (
	"fmt"
	"gitlab.com/jsco2t/go-logparse/internal/pkg/errors"
	"strconv"
	"time"
)

type LogLine struct {
	TimeStamp time.Time
	Username  string
	Operation string
	Size      int64
}

type LogLineHandlerFunc func(csvLine []string) (LogLine, error)

func LogLineHandler(csvLine []string) (LogLine, error) {
	if len(csvLine) != 4 {
		return LogLine{}, fmt.Errorf("%w: Column count must be 4", errors.ErrInvalidNumColumn)
	}

	size, err := strconv.ParseInt(csvLine[3], 10, 64)

	if err != nil {
		return LogLine{}, err
	}

	//timeStamp, err := time.Parse("Mon Jan _2 15:04:05 MST 2006", csvLine[0]) // UnixDate https://pkg.go.dev/time#Layout
	timeStamp, err := time.Parse(time.UnixDate, csvLine[0]) // UnixDate https://pkg.go.dev/time#Layout
	if err != nil {
		return LogLine{}, err
	}

	return LogLine{
		TimeStamp: timeStamp,
		Username:  csvLine[1],
		Operation: csvLine[2],
		Size:      size,
	}, nil
}
