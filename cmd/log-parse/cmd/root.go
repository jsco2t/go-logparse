package cmd

import (
	"github.com/spf13/cobra"
	"os"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "log-parse",
	Short:   "A simplified log parser",
	Long:    `A log parsing tool used for extracting details (user access date...etc) from a log file.`,
	Version: "0.1.0",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	//rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.log-parse.yaml)")
	rootCmd.PersistentFlags().StringP("log-file", "f", "./log.csv", "Path to log file")
	rootCmd.PersistentFlags().StringP("on-date", "d", "", "Only return results matching date (format RFC3339)")
}
