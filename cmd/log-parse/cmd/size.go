package cmd

import (
	"gitlab.com/jsco2t/go-logparse/pkg/handlers"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"os"
	"strconv"

	"github.com/spf13/cobra"
)

// sizeCmd represents the size command
var sizeCmd = &cobra.Command{
	Use:     "size <int>",
	Short:   "Search for records which have a specific size (in kilobytes)",
	Aliases: []string{"z"},
	Args:    cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		logFileName, _ := cmd.Flags().GetString("log-file")
		dateFilter, _ := cmd.Flags().GetString("on-date")
		operatorFilter, _ := cmd.Flags().GetString("operator")
		sizeFilter, _ := strconv.ParseInt(args[0], 10, 64)

		context := &request.RequestContext{}
		context.Filters.SizeFilter = sizeFilter
		context.Filters.DateFilter = dateFilter
		context.FilePath = logFileName

		if operatorFilter == "lt" {
			context.Filters.Operator = request.LT
		} else if operatorFilter == "eq" {
			context.Filters.Operator = request.EQ
		} else {
			context.Filters.Operator = request.GT
		}

		if cmd.Parent().Use == "summary" {
			context.Action = request.Summary
		} else {
			context.Action = request.Search
		}

		return handlers.Handle(context, os.Stdout)
	},
}

func init() {
	summaryCmd.AddCommand(sizeCmd)
	sizeCmd.PersistentFlags().StringP("operator", "o", "gt", "must be: gt, lt, eq")

	// to use the same command for two parents you have to make a copy
	var sizeCmdCopy = *sizeCmd
	searchCmd.AddCommand(&sizeCmdCopy)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// sizeCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// sizeCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
