package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/jsco2t/go-logparse/pkg/handlers"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"os"
)

// userCmd represents the user command
var userCmd = &cobra.Command{
	Use:     "user <username>",
	Aliases: []string{"u"},
	Short:   "Search for a specific user",
	Args:    cobra.MinimumNArgs(0),
	RunE: func(cmd *cobra.Command, args []string) error {
		logFileName, _ := cmd.Flags().GetString("log-file")
		dateFilter, _ := cmd.Flags().GetString("on-date")

		context := &request.RequestContext{}
		if len(args) > 0 {
			context.Filters.UserFilter = args[0]
		}
		context.Filters.DateFilter = dateFilter
		context.FilePath = logFileName

		if cmd.Parent().Use == "summary" {
			context.Action = request.Summary
		} else {
			context.Action = request.Search
		}

		return handlers.Handle(context, os.Stdout)
	},
}

func init() {
	summaryCmd.AddCommand(userCmd)

	// to use the same command for two parents you have to make a copy
	var userCmdCopy = *userCmd
	searchCmd.AddCommand(&userCmdCopy)
}
