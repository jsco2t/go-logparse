package cmd

import (
	"github.com/spf13/cobra"
)

// summaryCmd represents the summary command
var summaryCmd = &cobra.Command{
	Use:   "summary",
	Short: "Get summary results from the log file",
}

func init() {
	rootCmd.AddCommand(summaryCmd)
}
