package request

import (
	"fmt"
	"time"
)

type ActionType int64

const (
	Summary ActionType = iota
	Search
)

type OperatorType int64

const (
	LT OperatorType = iota
	GT
	EQ
)

type Filters struct {
	SizeFilter int64
	UserFilter string
	DateFilter string
	Operator   OperatorType
}

type RequestContext struct {
	Action   ActionType
	Filters  Filters
	FilePath string
}

func (r *RequestContext) IsSearch() bool {
	if r.Action == Search {
		return true
	}
	return false
}

func (r *RequestContext) IsSummary() bool {
	if r.Action == Summary {
		return true
	}
	return false
}

func (r *RequestContext) IsValid() bool {
	if (r.Action == Summary || r.Action == Search) && len(r.FilePath) > 0 {
		return true
	}
	return false
}

func (r *RequestContext) DateFilterToTime() (time.Time, bool) {
	resultTime := time.Now()

	if len(r.Filters.DateFilter) > 0 {
		resultTime, err := time.Parse("Mon Jan _2 15:04:05 MST 2006", r.Filters.DateFilter)
		if err != nil {
			fmt.Printf("Failed to convert supplied date to time object: %s, %s\n", r.Filters.DateFilter, err)
		}

		// we only support searching to a day level resolution
		resultTime = time.Date(
			resultTime.Year(),
			resultTime.Month(),
			resultTime.Day(),
			0,
			0,
			0,
			0,
			time.UTC,
		)

		return resultTime, true
	}

	return resultTime, false
}

func (o *OperatorType) String() string {
	if *o == GT {
		return fmt.Sprintf("gt")
	} else if *o == LT {
		return fmt.Sprintf("lt")
	} else {
		return fmt.Sprintf("eq")
	}
}
