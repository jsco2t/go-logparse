package handlers

import (
	"errors"
	"fmt"
	interrors "gitlab.com/jsco2t/go-logparse/internal/pkg/errors"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"io"
	"os"
)

type ValidationHandler struct {
	next RequestHandler
}

func (v *ValidationHandler) Handle(context *request.RequestContext, out io.Writer) error {
	// check that context is valid
	if context == nil {
		return fmt.Errorf("%w: supplied request context was nil", interrors.ErrInvalidReqContext)
	}

	if !context.IsValid() {
		return interrors.ErrInvalidReqContext
	}

	// check that log file exists:
	if _, err := os.Stat(context.FilePath); errors.Is(err, os.ErrNotExist) {
		return err
	}

	if v.next != nil {
		return v.next.Handle(context, out)
	}
	return nil
}

func (v *ValidationHandler) RegisterNext(requestHandler RequestHandler) {
	v.next = requestHandler
}
