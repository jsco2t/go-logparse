package handlers

import (
	"fmt"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"io"
)

type SearchHandler struct {
	next RequestHandler
}

func (s *SearchHandler) Handle(context *request.RequestContext, out io.Writer) error {
	if context.Action == request.Search {
		_, _ = fmt.Fprintln(out, "[NOT IMPLEMENTED - FUTURE FUNCTIONALITY]")
	}

	if s.next != nil {
		return s.next.Handle(context, out)
	}
	return nil
}

func (s *SearchHandler) RegisterNext(requestHandler RequestHandler) {
	s.next = requestHandler
}
