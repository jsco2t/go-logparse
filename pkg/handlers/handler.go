package handlers

import (
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"io"
)

type handlerChain struct {
	start RequestHandler
}

func Handle(context *request.RequestContext, out io.Writer) error {
	startHandler := registerHandlers()

	return startHandler.Handle(context, out)
}

func registerHandlers() RequestHandler {
	// in a chain of responsibility chain - you build the chain
	// from last to first in terms of the order of execution
	search := &SearchHandler{}

	summary := &SummaryHandler{}
	summary.RegisterNext(search)

	validation := &ValidationHandler{}
	validation.RegisterNext(summary)

	return validation
}
