package handlers

import (
	"fmt"
	"gitlab.com/jsco2t/go-logparse/internal/pkg/domain"
	csv "gitlab.com/jsco2t/go-logparse/pkg/processor"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"gitlab.com/jsco2t/go-logparse/pkg/results"
	"io"
	"os"
)

type SummaryHandler struct {
	next RequestHandler
}

func (s *SummaryHandler) Handle(context *request.RequestContext, out io.Writer) error {
	if context.Action == request.Summary {
		f, err := os.Open(context.FilePath)

		if err != nil {
			return fmt.Errorf("cannot open file: %w", err)
		}

		summaryResults := &results.SummaryResult{}
		if err := csv.ToResult(f, domain.LogLineHandler, context, summaryResults.Update); err != nil {
			return err
		}

		if err != nil {
			return err
		}

		if err := f.Close(); err != nil {
			return err
		}

		if _, err := fmt.Fprintf(out, "%v\n", summaryResults); err != nil {
			return err
		}
	}

	if s.next != nil {
		return s.next.Handle(context, out)
	}
	return nil
}

func (s *SummaryHandler) RegisterNext(requestHandler RequestHandler) {
	s.next = requestHandler
}
