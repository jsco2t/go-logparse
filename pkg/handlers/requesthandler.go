package handlers

import (
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"io"
)

type RequestHandler interface {
	Handle(context *request.RequestContext, out io.Writer) error
	RegisterNext(requestHandler RequestHandler)
}
