package csv

import (
	"encoding/csv"
	"fmt"
	"gitlab.com/jsco2t/go-logparse/internal/pkg/domain"
	"gitlab.com/jsco2t/go-logparse/internal/pkg/errors"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"gitlab.com/jsco2t/go-logparse/pkg/results"
	"io"
)

func ToResult(
	r io.Reader,
	lineHandler domain.LogLineHandlerFunc,
	context *request.RequestContext,
	resultProcessor results.ResultFunc,
) error {
	cr := csv.NewReader(r)
	cr.ReuseRecord = true // reuse the same slice for each read op to save mem - only use if reading by line

	for i := 0; ; i++ {
		row, err := cr.Read() // processing csv file line by line as a perf/memory improvement

		if err == io.EOF && i == 0 {
			return fmt.Errorf("%w: input file was empty", errors.ErrFileEmpty)
		}

		if err == io.EOF {
			break // hit eof
		}

		if err != nil {
			return fmt.Errorf("cannot read data from file: %w", err)
		}

		if i == 0 { // first column is header in processor
			continue
		}

		logLine, err := lineHandler(row)
		if err != nil {
			return fmt.Errorf("%w: %s", errors.ErrInvalidCsvFile, err)
		}

		err = resultProcessor(&logLine, context)
		if err != nil {
			return err
		}
	}

	return nil
}
