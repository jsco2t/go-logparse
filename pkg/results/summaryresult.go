package results

import (
	"fmt"
	"gitlab.com/jsco2t/go-logparse/internal/pkg/domain"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
	"strings"
	"time"
)

type SummaryResult struct {
	Users        map[string]int /* [name]count */
	SizeMatches  int64
	SizeFilter   int64
	SizeOperator string
	DateFilter   string
}

func (s *SummaryResult) Update(logLine *domain.LogLine, context *request.RequestContext) error {
	if s.Users == nil {
		s.Users = make(map[string]int)
		s.SizeOperator = context.Filters.Operator.String()
	}

	dateFilter, useFilter := context.DateFilterToTime()
	logTime := time.Date(
		logLine.TimeStamp.Year(),
		logLine.TimeStamp.Month(),
		logLine.TimeStamp.Day(),
		0,
		0,
		0,
		0,
		time.UTC,
	)

	// short circuit if date filter isn't matching
	if useFilter == true && dateFilter != logTime {
		return nil
	} else if useFilter == true {
		s.DateFilter = dateFilter.String()
	}

	// using a user filter
	if len(context.Filters.UserFilter) > 0 {
		if logLine.Username == context.Filters.UserFilter {
			s.Users[logLine.Username]++
		}
	} else if context.Filters.SizeFilter == 0 {
		s.Users[logLine.Username]++
	}

	// use a size filter
	if context.Filters.SizeFilter > 0 {
		s.SizeFilter = context.Filters.SizeFilter
		if context.Filters.Operator == request.GT {
			if logLine.Size > context.Filters.SizeFilter {
				s.SizeMatches++
			}
		} else if context.Filters.Operator == request.LT {
			if logLine.Size < context.Filters.SizeFilter {
				s.SizeMatches++
			}
		} else {
			if logLine.Size == context.Filters.SizeFilter {
				s.SizeMatches++
			}
		}
	}

	return nil
}

func (s *SummaryResult) String() string {
	//var summary string
	var summary strings.Builder
	summary.WriteString(fmt.Sprintf("[SUMMARY RESULTS]\n"))

	// use summary report
	if len(s.Users) > 0 {
		summary.WriteString(fmt.Sprintf("Users summary: (users found: %d)\n", len(s.Users)))

		for key, value := range s.Users {
			summary.WriteString(fmt.Sprintf("\tName: %s, Result: %d (request count)\n", key, value))
		}
		if len(s.DateFilter) > 0 {
			summary.WriteString(fmt.Sprintf("\tUsed date filter: %s\n", s.DateFilter))
		}
	} else {
		summary.WriteString(fmt.Sprintf("--no user matches found--\n"))
	}

	// size summary report
	if s.SizeFilter > 0 {
		summary.WriteString(fmt.Sprintf("Request size summary: \n"))
		summary.WriteString(fmt.Sprintf(
			"\tFilter size of %d, Result: %d (request count '%s' filter)\n",
			s.SizeFilter,
			s.SizeMatches,
			s.SizeOperator,
		))
		if len(s.DateFilter) > 0 {
			summary.WriteString(fmt.Sprintf("\tUsed date filter: %s\n", s.DateFilter))
		}
	}

	return summary.String()
}
