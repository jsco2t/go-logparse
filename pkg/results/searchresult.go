package results

import (
	"gitlab.com/jsco2t/go-logparse/internal/pkg/domain"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
)

type SearchResult struct {
	LogLines []string
}

func (s *SearchResult) Update(logLine *domain.LogLine, context *request.RequestContext) error {
	return nil
}
