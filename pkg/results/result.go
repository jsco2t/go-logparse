package results

import (
	"gitlab.com/jsco2t/go-logparse/internal/pkg/domain"
	"gitlab.com/jsco2t/go-logparse/pkg/request"
)

type ResultProcessor interface {
	Update(logLine *domain.LogLine, context *request.RequestContext) error
}

type ResultFunc func(logLine *domain.LogLine, context *request.RequestContext) error
